package de.dwd.ti.metgate;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;

public class SparkZugang {

	   private static final String connectString = "jdbc:hive2://oflxs816.dwd.de:10000";

	   public static void main(String[] args) throws ClassNotFoundException, SQLException, IOException {
		   	
		   	 //BasicConfigurator.configure();
		     String log4jConfPath = "C:\\Users\\sberinge\\eclipse-workspace\\ApacheSparkTest\\logging.properties";
		     PropertyConfigurator.configure(log4jConfPath);
             // Datenbankverbindung herstellen
             Class.forName("org.apache.hive.jdbc.HiveDriver");
             Connection conn = DriverManager.getConnection(connectString, "no_username", "no_password");
             conn.setAutoCommit(false);
             
             System.out.println("\nExisting tables in Spark:\n");
             
             Statement stmt = conn.createStatement();
             ResultSet res = stmt.executeQuery("show tables in default");
             while (res.next()) {
                   System.out.println(res.getString(2));
             }
             
             //ResultSet res2 = stmt.executeQuery("select * from met_parameter");
             
             //while (res2.next()) {
              //   System.out.println(res2.getString(2));
             //}
             
             // close connection and terminate
             conn.close();
             System.out.println("\nConnection and Testing successful!\nReady for further processings...");                                  
	   }

}
